---
layout: 2020/post
section: propuestas
category: talks
title: Todo lo que necesitas saber sobre desarrollo de Apps con Ubuntu Touch
state: confirmed
---

Ubuntu Touch es un sistema operativo móvil 100% GNU/Linux que maneja la convergencia, es decir, poder usar tu dispositivo móvil como un computador, una tablet o como un smartphone para todas las tareas cotidianas. Actualmente se encuentra soportado por ciertos dispositivos que se encuentra en fase madura y experimentación para el uso de la misma.

Además cuenta con un desarrollo móvil más ligero y poco pesado para sistemas operativos desktops o distros GNU/Linux, lo que permite que el desarrollador no tarde horas compilando una aplicación o tarde tiempo en compilar la misma.

## Formato de la propuesta

Indicar uno de estos:

* [ ] &nbsp;Charla (25 minutos)
* [x] &nbsp;Charla relámpago (10 minutos)

## Descripción

Explicaré lo que realmente necesitas para desarrollar aplicaciones web bajo Ubuntu Touch, realmente no necesitas ser un desarrollador experimentado o "senior" para desarrollar aplicaciones móviles bajo el sistema operativo Ubuntu Touch que es 100% GNU/Linux, incluso tampoco necesitas un dispositivo, solo necesitas tener un desktop, ganas y motivación.

## Público objetivo

Desarrolladores o cualquier persona interesada en el tema.

## Ponente(s)

-   **Lina Castro** - Colombia.
  -   Desarrolladora de software con especialización en desarrollo de aplicaciones móviles, Coach en Platzi, entusiasta de Linux, hago parte del equipo que desarrolla aplicaciones moviles para Ubuntu Touch en Ubports. Lider de la comunidad de Ubuntu en Colombia, miembro del Ubuntu Council de mi comunidad.


### Contacto(s)

-   **Lina Castro**
     - GitLab: @linacastrodev
     - Twitter: @lirrums

## Comentarios
-

## Condiciones

* [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
