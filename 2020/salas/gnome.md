---
layout: 2020/post
section: propuestas
category: devrooms
title: GNOME&#58 La experiencia de escritorio ética y elegante
state: confirmed
---

Como indica la [Wikipedia](https://es.wikipedia.org/wiki/GNOME), GNOME es un entorno de escritorio e infraestructura de desarrollo para sistemas operativos GNU/Linux, Unix y derivados Unix como BSD o Solaris; compuesto enteramente de software libre.

El proyecto fue iniciado por los programadores mexicanos Miguel de Icaza y Federico Mena en agosto de 1997 y forma parte oficial del proyecto GNU. Nació como una alternativa a KDE bajo el nombre de GNU Network Object Model Environment (Entorno de Modelo de Objeto de Red GNU). Actualmente, incluyendo al español, se encuentra disponible en 166 idiomas.

GNOME está disponible en las principales distribuciones GNU/Linux, incluyendo Fedora, Debian, Ubuntu, Red Hat Linux, CentOS, Oracle Linux, Arch Linux y Gentoo.

El objetivo de esta sala es dar a conocer el [escritorio GNOME](https://www.gnome.org/), el proyecto, la fundación que lo apoya, e iniciativas como GNOME Hispano.

Se pretende dar una visión práctica, desde el punto de vista de persona usuaria a personas con conocimientos técnicos como para poder contribuir de forma técnica al proyecto. También se prentende mostrar otras vías de contribución (documentación, traducción, etc.), así como iniciativas de apoyo al desarrollo como [Google Summer of Code](https://summerofcode.withgoogle.com/archive/2019/organizations/6458159500099584/) ó [Outreachy](https://www.outreachy.org/).

## Comunidad o grupo que lo propone

La propuesta se hace desde la comunidad de **GNOME Hispano**, que agrupa sus publicaciones en un [_planeta_](http://planeta.es.gnome.org/) y en su versión [en inglés](https://planet.gnome.org/), se coordina a través de un canal de Telegram, ha organizado el encuentro anual de la comunidad hispana GUADEC Hispano, y ha colaborado en la organización del encuentro internacional [GUADEC](https://events.gnome.org/event/1/). Para más información del proyecto visitar el [wiki](https://wiki.gnome.org/).

### Contactos

-   **María Majadas**: majadas.maria at gmail dot com
-   **José Manrique López de la Fuente**: jsmanrique at gmail dot com | @jsmanrique

## Público objetivo

Como se indicaba anteriormente, el público objetivo es cualquier persona que quiera tanto usar como contribuir al proyecto de escritorio GNOME.

## Tiempo

Día completo.

## Día

En principio, es indiferente que se haga el primer o el segundo día.

## Formato

La sala se plantea como la siguiente sucesión de charlas ya previstas por la comunidad GNOME:

-   **Mañana**:
    -   **11:00**: **Daniel Garcia Moreno**. Flatpak para programadores: Introducción al uso de Flatpak para compilar, depurar, CI, etc.
    -   **12:00**: **Carlos Garnacho**. GTK4: 2020 tenía que traer algo bueno, a finales de este año GTK 4.0 verá la luz. Esta charla repasará las novedades y cambios de paradigma comparado con GTK3.
    -   **13:00**: **Florian Mullner**. GNOME Shell (Descripción próximamente).
-   **Tarde**:
    -   **16:00**: **Taller Newcomers**: Aprende a hacer tu primera contribución en GNOME (las personas interesadas en este taller deberían dirigirse previamente al **grupo de Telegram de [GNOME Hispano](https://t.me/joinchat/AOj4fAM8EkCnsCYjwV6t7A)**).
    -   **18:00**: **Alvaro Peña**. Tecnologías de GNOME en entornos embebidos. Experiencias en la industria, caso de uso, desusos y perspectivas.

## Comentarios

Nada relevante.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que proponen la devroom estará presente el día agendado para la _devroom_.
-   [x] &nbsp;Acepto coordinarme con la organización de esLibre.
-   [x] &nbsp;Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la _devroom_ podría retirarse.
